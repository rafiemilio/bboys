﻿Shader "Unlit/Menu/Water"
{
	//Properties are like the public variables of a shader (what you see in inspector)

	Properties
	{
		// _MainText Name of the variable
		// "Texture" isthe text lable in unity inspector
		// 2D is the actual variable type
		// white is the default color

		_MainTex ("Texture", 2D) = "white" {}
		_WaveHeight ("Wave Height Yea", Float ) = 1.0
		_WaveRepeat ("Wave Repeat" , Float ) = 1.0
	}


	// SubShader is where your shader code acually goes

	SubShader
	{
		//Opaque means not transparent which is the fastest to draw
		Tags { "RenderType"="Opaque" }
		
		// Level of detail (higher number is more expensive)
		LOD 100

		// Pass is 1 draw call, kind of
		// ideally shaders are single-pass, just 1 pass

		Pass
		{
			//CGPROGRAM means the start of the shader code

			CGPROGRAM

			//pragma is greek for action, means special Special Instructions

			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			

			//cginc means cg include its another piece of shader code
			#include "UnityCG.cginc"

			//struct is like a small class for data
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			//vertex to fragment
			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			// 2 variables
			// declare a "twin" of your public property (from the top of your shader)

			sampler2D _MainTex;
			float _WaveHeight;
			float _WaveRepeat;

			//ST stands for Scale/Translate (tiling and offset)
			float4 _MainTex_ST;

			//_Time special number that contains current time
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.vertex += float4(0,sin((_Time.y + o.vertex.z) * _WaveRepeat) * _WaveHeight,0,0);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv + fixed2(_Time.y * .07, _Time.y *.02));
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG

			//ENDCG is where the shader code ends
		}
	}
}
