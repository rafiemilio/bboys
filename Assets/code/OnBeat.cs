﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnBeat : MonoBehaviour {
	SpriteRenderer image;

	// Use this for initialization
	void Start () {
		image = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other) {
		image.enabled = true;
		Song.onBeat = true;
	}

	void OnTriggerExit2D(Collider2D other) {
		image.enabled = false;
		Song.onBeat = false;
	}
}
