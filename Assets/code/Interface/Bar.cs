﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Bar : MonoBehaviour {

	static public float player1 = .5f;
	static public float player2 = .5f;

	public bool playerOne;

	float speed = 5;
	float diff;

	Image image;

	// Use this for initialization
	void Start () {
		player1 = .5f;
		player2 = .5f;

		image = GetComponent<Image>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (playerOne) {

			diff = player1 - image.fillAmount;

			if (player1 > image.fillAmount) {
				image.fillAmount += speed * Time.deltaTime * diff;
			}

			if (player1 < image.fillAmount) {
				image.fillAmount -= -speed * Time.deltaTime * diff;
			}

		} else {

			diff = player2 - image.fillAmount;

			if (player2 > image.fillAmount) {
				image.fillAmount += speed * Time.deltaTime * diff;
			}

			if (player2 < image.fillAmount) {
				image.fillAmount -= -speed * Time.deltaTime * diff;
			}
		}
		 
	
	}
}
