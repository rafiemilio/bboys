﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FightOverlay : MonoBehaviour {

	public GameObject fight;
	float waitTime;
	float startGame;


	float fillSpeed = .5f;

	// Use this for initialization
	void Start () {

		if (Song.chosenSong == 1) {
			waitTime = Song.note * 1.25f;
		}
		else if (Song.chosenSong == 2) {
			waitTime = Song.note * 5.5f;
		}

		else if (Song.chosenSong == 2) {
			waitTime = Song.note * 5.5f;
		}

		else if (Song.chosenSong == 3) {
			waitTime = 0;
		}

		else if (Song.chosenSong == 4) {
			waitTime = Song.note * 3.5f;
		}

		startGame = Song.note * .5f;

		StartCoroutine("Play");

	}
	
	void Update () {

	}


	IEnumerator Play () {
		yield return new WaitForSeconds(waitTime);
		
		print("start");

		yield return new WaitForSeconds(startGame);
		fight.SetActive(true);
	}
	
}
