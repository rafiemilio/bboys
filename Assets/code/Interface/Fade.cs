﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fade : MonoBehaviour {

	public Image image;
	float alphaFade = 1;
	public float fadeSpeed = .5f;

	// Update is called once per frame
	void Update () {
	
		if (alphaFade > 0) {
			alphaFade -= fadeSpeed * Time.deltaTime;
		} else {
			Destroy(gameObject);
		}

		image.color = new Color(0,0,0,alphaFade);
	}
}
