﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BeatCounterOld : MonoBehaviour {

	float delay;
	float wait;
	public FadeOut beat;
	public int note;
	
	public bool first;
	public AudioSource audio;

	void Awake () {
		

		if (note == 1) {
			wait = 0;
			delay = Song.note;
		}
		
		if (note == 2) {
			wait = Song.note * .25f;
			delay = Song.note;
		}
		if (note == 3) {
			wait = Song.note * .5f;
			delay = Song.note;
		}
		if (note == 4) {
			wait = Song.note * .75f;
			delay = Song.note;
		}
		
	}
	
	void Start () {

		InvokeRepeating("Pulse", wait, delay);

	}


	void Pulse () {

		beat.alphaFade = 1;
		/*
		if (first) {
			if (beat.alphaFade == 1) {
				if (!audio.isPlaying) {
					audio.Play();
				}
				
			}
		}
		*/

	
	}
}
