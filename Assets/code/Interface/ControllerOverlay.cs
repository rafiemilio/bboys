﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControllerOverlay : MonoBehaviour {

	public bool LP;
	public bool MP;
	public bool HP;
	public bool LK;
	public bool	MK;
	public bool HK;
	public bool Left;
	public bool Down;
	public bool Right;
	public bool Up;

	public Sprite button;
	public Sprite buttonDown;

	Image image;
	public Color colorA;
	public Color colorB;

	public int player = 1;

	// Use this for initialization
	void Start () {

		image = GetComponent<Image>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (LP) {
			if (Input.GetButton("P"+player+"_LP")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (MP) {
			if (Input.GetButton("P"+player+"_MP")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (HP) {
			if (Input.GetButton("P"+player+"_HP")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (LK) {
			if (Input.GetButton("P"+player+"_LK")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (MK) {
			if (Input.GetButton("P"+player+"_MK")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (HK) {
			if (Input.GetButton("P"+player+"_HK")) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}

		if (Left) {
			if (Input.GetAxis("P"+player+"_Horizontal") < 0) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (Right) {
			if (Input.GetAxis("P"+player+"_Horizontal") > 0) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (Down) {
			if (Input.GetAxis("P"+player+"_Vertical") > 0) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}
		if (Up) {
			if (Input.GetAxis("P"+player+"_Vertical") < 0) {
				image.sprite = buttonDown;
				image.color = colorB;
			} else {
				image.sprite = button;
				image.color = colorA;
			}
		}

	
	}
}
