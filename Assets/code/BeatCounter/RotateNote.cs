﻿using UnityEngine;
using System.Collections;

public class RotateNote : MonoBehaviour {

	public bool reverse;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (reverse) {
			transform.Rotate(0,-10,0);
		} else {
			transform.Rotate(0,10,0);
		}

		
	
	}
}
