﻿using UnityEngine;
using System.Collections;

public class Train : MonoBehaviour {

	Vector3 move;
	bool shook;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		move = transform.position;

		move.x += 100 * Time.deltaTime;

		if (move.x > 500) {
			move.x = -3000;
			shook = false;
		}

		if (move.x > -100 && !shook) {
			Shake.now = true;
			shook = true;
		}


		move.y = Random.Range(1.8f, 2.3f);


		transform.position =  move;
	
	}
}
