﻿using UnityEngine;
using System.Collections;

public class MoveName : MonoBehaviour {

	tk2dTextMesh text;

	public Player playerscript;

	Renderer renderer;

	float alpha;

	// Use this for initialization
	void Start () {
		text = GetComponent<tk2dTextMesh>();

		renderer = GetComponent<Renderer>();
	
	}
	
	// Update is called once per frame
	void Update () {


		if (playerscript.move == 1) {
			text.text = "TOP ROCK";
			alpha = 1;
			playerscript.move = 0;
		}

		if (playerscript.move == 2) {
			text.text = "DROP";
			alpha = 1;
			playerscript.move = 0;
		}

		if (playerscript.move == 3) {
			text.text = "WINDMILL";
			alpha = 1;
			playerscript.move = 0;
		}

		if (playerscript.move == 4) {
			text.text = "FLARE";
			alpha = 1;
			playerscript.move = 0;
		}

		if (playerscript.move == 5) {
			text.text = "HANDSPIN";
			alpha = 1;
			playerscript.move = 0;
		}

		if (playerscript.move == 6) {
			text.text = "FREEZE";
			alpha = 1;
			playerscript.move = 0;
		}

		if (alpha > 0) {
			alpha -= .5f * Time.deltaTime;
		}
		
		text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
	}
}
