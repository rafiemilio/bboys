﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerBar : MonoBehaviour {

	static public float player1 = .5f;
	static public float player2 = .5f;

	public bool playerOne;
	public bool A;
	public bool B;
	public bool C;

	float speed = 5;
	float diff;

	Image image;

	// Use this for initialization
	void Start () {
		player1 = 0;
		player2 = 0;

		image = GetComponent<Image>();
	
	}
	
	// Update is called once per frame
	void Update () {

		print(player1);

		if (playerOne) {
			//player1 += .1f * Time.deltaTime;

			if (player1 < 0) {
				player1 = 0;
			}
			if (player1 > 3) {
				player1 = 3;
			}
			//if (Input.GetButtonDown("P1_LP")) {
			//	player1 -= .5f;
			//	player2 -= 1;
			//}
		} else {
			//player2 += .1f * Time.deltaTime;

			if (player2 < 0) {
				player2 = 0;
			}
			if (player2 > 3) {
				player2 = 3;
			}
		}


		

		if (playerOne) {

			if (A) {
				if (player1 < 1) {

					diff = player1 - image.fillAmount;

					if (player1 > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if (player1 < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}

			if (B) {
				if (player1 < 2) {

					diff = (player1 - 1) - image.fillAmount;

					if ((player1 - 1) > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if ((player1 - 1) < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}

			if (C) {
				if (player1 < 3) {

					diff = (player1 - 2) - image.fillAmount;

					if ((player1 - 2) > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if ((player1 - 2) < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}

			
		} else {

			if (A) {
				if (player2 < 1) {

					diff = player2 - image.fillAmount;

					if (player2 > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if (player2 < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}

			if (B) {
				if (player2 < 2) {

					diff = (player2 - 1) - image.fillAmount;

					if ((player2 - 1) > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if ((player2 - 1) < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}

			if (C) {
				if (player2 < 3) {

					diff = (player2 - 2) - image.fillAmount;

					if ((player2 - 2) > image.fillAmount) {
						image.fillAmount += speed * Time.deltaTime * diff;
					}

					if ((player2 - 2) < image.fillAmount) {
						image.fillAmount -= -speed * Time.deltaTime * diff;
					}

				} else {
					image.fillAmount = 1;
				}
			}
		}
		 
	
	}
}
