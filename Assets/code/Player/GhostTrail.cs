﻿using UnityEngine;
using System.Collections;

public class GhostTrail : MonoBehaviour {

	tk2dSprite sprite;
    tk2dSpriteAnimator anim;

    Renderer renderer;

    public int player = 1;

	string spriteName;
	void Start() {
        anim = GetComponent<tk2dSpriteAnimator>();
	 	sprite = GetComponent<tk2dSprite>();
        renderer = GetComponent<Renderer>();

        InvokeRepeating("SpawnTrail", 0, .05f);
    }

    void Update() {

    }

    void SpawnTrail() {

        if (player == 1 ) {
            if (Player.player1Power) {

                GameObject trailPart = new GameObject();
                tk2dSprite trailPartRenderer = trailPart.AddComponent<tk2dSprite>();
                trailPartRenderer.Collection = sprite.Collection;
                trailPartRenderer.SetSprite(sprite.CurrentSprite.name);
                if (player == 1) {
                    trailPartRenderer.scale = new Vector3(2,2,2);
                } else {
                    trailPartRenderer.scale = new Vector3(-2,2,2);
                }
            
                trailPart.transform.position = transform.position;
                Destroy(trailPart, 0.2f); 
     
                StartCoroutine("FadeTrailPart", trailPartRenderer);
            }
        } else {
            if (Player.player2Power) {

                GameObject trailPart = new GameObject();
                tk2dSprite trailPartRenderer = trailPart.AddComponent<tk2dSprite>();
                trailPartRenderer.Collection = sprite.Collection;
                trailPartRenderer.SetSprite(sprite.CurrentSprite.name);
                if (player == 1) {
                    trailPartRenderer.scale = new Vector3(2,2,2);
                } else {
                    trailPartRenderer.scale = new Vector3(-2,2,2);
                }
            
                trailPart.transform.position = transform.position;
                Destroy(trailPart, 0.2f); 
     
                StartCoroutine("FadeTrailPart", trailPartRenderer);
            }
        }
        
        
        
    }
 
    IEnumerator FadeTrailPart(tk2dSprite trailPartRenderer) {
        
        Color color = new Color (1,1,1);
/*
        if (player == 1) {
            color = new Color (1,.6f,0f);
        } else {
            color = new Color (.05f,.65f,.98f);
        }
       */

                    color.a -= .8f;
        //color.a -= .8f; // replace 0.5f with needed alpha decrement
        trailPartRenderer.color = color;
 
        yield return new WaitForEndOfFrame();

    }
}
