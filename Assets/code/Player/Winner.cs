﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Winner : MonoBehaviour {

	Renderer renderer;
	public bool player1;

	// Use this for initialization
	void Start () {

		renderer = GetComponent<Renderer>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (player1) {
			if (Bar.player1 >= 1) {
				renderer.enabled = true;
				StartCoroutine("Reset");
			} else {
				renderer.enabled = false;
			}
 		} else {
 			if (Bar.player2 >= 1) {
				renderer.enabled = true;
				StartCoroutine("Reset");
			} else {
				renderer.enabled = false;
			}
 		}
	}

	IEnumerator Reset() {
		yield return new WaitForSeconds(2);
		Application.LoadLevel(0);
	}
}
