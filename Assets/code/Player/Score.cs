﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	tk2dTextMesh text;

	public Player playerscript;
	public bool combo;

	Renderer renderer;

	// Use this for initialization
	void Start () {
		text = GetComponent<tk2dTextMesh>();

		renderer = GetComponent<Renderer>();
	
	}
	
	// Update is called once per frame
	void Update () {


		if (combo) {
			if (playerscript.currentScore > 1) {
				renderer.enabled = true;
				if (playerscript.multiply > 1) {
					text.text = playerscript.currentScore.ToString() + " X "+ playerscript.multiply.ToString();
				} else {
					text.text = playerscript.currentScore.ToString();

				}
			} else {
				renderer.enabled = false;
			}
		} else {
			text.text = playerscript.score.ToString();
		}
		
	}
}
