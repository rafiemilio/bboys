﻿using UnityEngine;
using System.Collections;

public class PowerName : MonoBehaviour {

	tk2dTextMesh text;

	float alpha;

	public bool playerOne;

	static public float size = 1;


	// Use this for initialization
	void Start () {
		
		size = 1;
		text = GetComponent<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {


		if (playerOne) {

			if (PowerBar.player1 > 1) {
				alpha = 1;
			} else {

				if (size > 1) {
					alpha = 1;
				} else {
					alpha = .2f;
				}
				
			}

		} else {

			if (PowerBar.player2 > 1) {
				alpha = 1;
			} else {
				if (size > 1) {
					alpha = 1;
				} else {
					alpha = .2f;
				}
			}

		}

		if (size > 1) {
			text.color = new Color (1, .5f, .5f, alpha);
			size -= 2 * Time.deltaTime;
		} else {
			text.color = new Color (1, 1, 1, alpha);
			size = 1;
		}

		transform.localScale = new Vector3(size,size,size);

	
	}
}
