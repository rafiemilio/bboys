﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	tk2dSprite sprite;
	tk2dSpriteAnimator anim;
	public int player = 1;

	bool down;
	
	//MOVES//
	int topRockState;
	int freezeState;
	int powerState;

	int dropState;
	int windmillState;
	int windmillStick;

	//MOVES//

	public int comboScore;
	public int currentScore;
	int countUp;
	int countDiff;
	public int score;
	public int multiply;
	static public bool player1Power;
	static public bool player2Power;

	public int move;

	public GameObject drawPower;

	int bar;

	// Use this for initialization
	void Start () {
		player1Power = false;
		player2Power = false;

		sprite = GetComponent<tk2dSprite>();
		anim = GetComponent<tk2dSpriteAnimator>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Game.started) {
			Moves();
		}

		if (score < countUp) {
			countDiff = countUp - score;

			if (countDiff > 10000) {
				score += 1000;
			} 

			else if (countDiff > 1000) {
				score += 100;
			}

			else if (countDiff > 100) {
				score += 10;
			}

			else if (countDiff < 100) {
				score++;
			}
			
		}

		if (multiply < 1 && currentScore > 0) {
			countUp = score + currentScore;
			currentScore = 0;
			if (player == 1 && bar > 0) {
				Bar.player1 += .01f * bar;
				Bar.player2 -= .01f * bar;
				bar = 0;
			} 

			else if (player == 2 && bar > 0) {
				Bar.player1 -= .01f * bar;
				Bar.player2 += .01f * bar;
				bar = 0;
			}
		} 


		if (multiply > 0) {

			bar = multiply;



			if (player == 1) {
				sprite.color = new Color(1,.6f,0);
			} else {
				sprite.color = new Color(.05f,.65f,.98f);
			}
			
		} else {
			if (!Input.GetButton ("P"+player+"_LP") &&
				!Input.GetButton ("P"+player+"_MP") &&
				!Input.GetButton ("P"+player+"_HP")) {

				sprite.color = new Color(1,1,1);
			}
		}
	
	}

	void Moves() {

		Toprock();
		Freeze();
		Power();


		//Drop();
		//Windmill();


	}



	void Toprock() {


		if (topRockState == 0 
			&& dropState == 0
			&& powerState == 0
			&& windmillState == 0 
			&& freezeState == 0) {

			

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
				if (drawPower.active) {
					drawPower.SetActive(false);
				}

			}

			if (Input.GetButtonDown ("P"+player+"_LP")) {
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
					if (!drawPower.active) {
						drawPower.SetActive(true);
					}
				} else {
					if (drawPower.active) {
						drawPower.SetActive(false);
					}
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;

					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

				anim.Play("TopRock_1");
				topRockState = 1;
				dropState = 0;

			}


		}

		else if (topRockState == 1) {

			if (!anim.IsPlaying("TopRock_1")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("TopRock_2");
				topRockState = 2;
				if (Song.onBeat) {
					if (!drawPower.active) {
						drawPower.SetActive(true);
					}
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					if (drawPower.active) {
						drawPower.SetActive(false);
					}
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}


		}


		else if (topRockState == 2) {

			if (!anim.IsPlaying("TopRock_2")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("TopRock_3");
				topRockState = 3;
				if (Song.onBeat) {
					if (!drawPower.active) {
						drawPower.SetActive(true);
					}
					Song.p1Combo = true;
					multiply += 1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					if (drawPower.active) {
						drawPower.SetActive(false);
					}
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}


		}

		else if (topRockState == 3) {

			if (!anim.IsPlaying("TopRock_3")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				topRockState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("TopRock_4");
				topRockState = 0;
				move = 1;
				if (Song.onBeat) {
					if (!drawPower.active) {
						drawPower.SetActive(true);
					}
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					if (drawPower.active) {
						drawPower.SetActive(false);
					}
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}

		}

	}

	void Power() {
		if (player == 1) {
			if (powerState > 1) {
				player1Power = true;
			} else {
				player1Power = false;
			}
		} else {
			if (powerState > 1) {
				player2Power = true;
			} else {
				player2Power = false;
			}
		}

		if (powerState == 0
			&& dropState == 0
			&& topRockState == 0
			&& windmillState == 0
			&& freezeState == 0) {

			

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			

			if (Input.GetButtonDown ("P"+player+"_HP")) {

				if (player == 1) {
					if (PowerBar.player1 > 1) {
						anim.Play("Power");
						topRockState = 0;
						powerState = 1;
						if (Song.onBeat) {
							Song.p1Combo = true;
							multiply +=1;
							currentScore += 10 * multiply;
						} else {
							sprite.color = new Color(1,0,0);
							Song.p1Combo = false;
							multiply = 0;
							if (player == 1) {
								Bar.player1 -= .02f;
								Bar.player2 += .02f;
							} 

							else if (player == 2) {
								Bar.player1 += .02f;
								Bar.player2 -= .02f;
							}
						}
					} else {
						PowerName.size = 1.5f;
					}
				} else {
					if (PowerBar.player2 > 1) {
						anim.Play("Power");
						topRockState = 0;
						powerState = 1;
						if (Song.onBeat) {
							Song.p1Combo = true;
							multiply +=1;
							currentScore += 10 * multiply;
						} else {
							sprite.color = new Color(1,0,0);
							Song.p1Combo = false;
							multiply = 0;
							if (player == 1) {
								Bar.player1 -= .02f;
								Bar.player2 += .02f;
							} 

							else if (player == 2) {
								Bar.player1 += .02f;
								Bar.player2 -= .02f;
							}
						}
					} else {
						PowerName.size = 1.5f;
					}

				}

				
			}

		}

		else if (powerState == 1) {

			if (Input.GetButtonDown ("P"+player+"_MP") || 
				Input.GetButtonDown ("P"+player+"_LP") ) {

				anim.Play("BBoyStance");
				powerState = 0;
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (!anim.IsPlaying("Power")) {
				powerState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			} else {
				if (player == 1) {

					PowerBar.player1 -= .3f * Time.deltaTime;
					if (PowerBar.player1 <= 0) {
						powerState = 0;
						anim.Play("BBoyStance");
						Song.p1Combo = false;
						multiply = 0;
					}
				} else {
					PowerBar.player2 -= .3f * Time.deltaTime;
					if (PowerBar.player2 <= 0) {
						powerState = 0;
						anim.Play("BBoyStance");
						Song.p2Combo = false;
						multiply = 0;
					}
				}
			} 
		}

	}

	void Drop() {
		
		if (player == 1) {
			if (dropState > 2) {
				player1Power = true;
			} else {
				player1Power = false;
			}
		} else {
			if (dropState > 2) {
				player2Power = true;
			} else {
				player2Power = false;
			}
		}

		if (dropState == 0
			&& topRockState == 0
			&& windmillState == 0
			&& freezeState == 0) {

			

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {

				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {

				if (player == 1) {
					if (PowerBar.player1 > 1) {
						anim.Play("Drop_1");
						topRockState = 0;
						dropState = 1;
						if (Song.onBeat) {
							Song.p1Combo = true;
							multiply +=1;
							currentScore += 10 * multiply;
						} else {
							sprite.color = new Color(1,0,0);
							Song.p1Combo = false;
							multiply = 0;
							if (player == 1) {
								Bar.player1 -= .02f;
								Bar.player2 += .02f;
							} 

							else if (player == 2) {
								Bar.player1 += .02f;
								Bar.player2 -= .02f;
							}
						}
					} else {
						PowerName.size = 1.5f;
					}
				} else {
					if (PowerBar.player2 > 1) {
						anim.Play("Drop_1");
						topRockState = 0;
						dropState = 1;
						if (Song.onBeat) {
							Song.p1Combo = true;
							multiply +=1;
							currentScore += 10 * multiply;
						} else {
							sprite.color = new Color(1,0,0);
							Song.p1Combo = false;
							multiply = 0;
							if (player == 1) {
								Bar.player1 -= .02f;
								Bar.player2 += .02f;
							} 

							else if (player == 2) {
								Bar.player1 += .02f;
								Bar.player2 -= .02f;
							}
						}
					} else {
						PowerName.size = 1.5f;
					}

				}

				
			}

		}

		else if (dropState == 1) {

			if (!anim.IsPlaying("Drop_1")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Drop_2");
				dropState = 2;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}


		}

		else if (dropState == 2) {

			if (!anim.IsPlaying("Drop_2")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Drop_3");
				move = 2;
				dropState = 3;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}


		}

		else if (dropState == 3) {

			if (!anim.IsPlaying("Drop_3")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_1");
				dropState = 4;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 4) {

			if (!anim.IsPlaying("Windmill_1")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_2");
				dropState = 5;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 5) {

			if (!anim.IsPlaying("Windmill_2")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_3");
				dropState = 6;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 6) {

			if (!anim.IsPlaying("Windmill_3")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_4");
				dropState = 7;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 7) {

			if (!anim.IsPlaying("Windmill_4")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_5");
				dropState = 8;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 8) {

			if (!anim.IsPlaying("Windmill_5")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_6");
				dropState = 9;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 9) {

			if (!anim.IsPlaying("Windmill_6")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_7");
				dropState = 10;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 10) {

			if (!anim.IsPlaying("Windmill_7")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_8");
				dropState = 11;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 11) {

			if (!anim.IsPlaying("Windmill_8")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_9");
				dropState = 12;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 12) {

			if (!anim.IsPlaying("Windmill_9")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_10");
				dropState = 13;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 13) {

			if (!anim.IsPlaying("Windmill_10")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_11");
				dropState = 14;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 14) {

			if (!anim.IsPlaying("Windmill_11")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_12");
				dropState = 15;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 15) {

			if (!anim.IsPlaying("Windmill_12")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_13");
				dropState = 16;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 16) {

			if (!anim.IsPlaying("Windmill_13")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_14");
				dropState = 17;
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}

		else if (dropState == 17) {

			if (!anim.IsPlaying("Windmill_14")) {
				dropState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_15");
				if (player == 1) {
					PowerBar.player1 -= .2f;
				} else {
					PowerBar.player2 -= .2f;
				}
				dropState = 0;
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
			}

		}


		if (dropState > 0) {

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				if (player == 1) {
					if (PowerBar.player1 < .01f) {
						anim.Play("BBoyStance");
						down = false;
						Song.p1Combo = false;
						multiply = 0;
					}
				} else {

					if (PowerBar.player2 < .01f) {
						anim.Play("BBoyStance");
						down = false;
						Song.p1Combo = false;
						multiply = 0;
					}

				}
			}

		}

	}

	void Windmill() {


		if (down 
			&& windmillState == 0
			&& topRockState == 0
			&& dropState == 0) {


			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill")) {
				anim.Play("BBoyStance");
				down = false;
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill");
				windmillState = 1;
				windmillStick = 0;
			}

		}

		else if (windmillState == 1) {

			if (!anim.IsPlaying("Windmill")) {
				
				anim.Play("Windmill");
				windmillState = 2;
			}


		}

		else if (windmillState == 2) {

			if (!anim.IsPlaying("Windmill")) {
				windmillState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_3");
				windmillState = 3;
			}

		}

		else if (windmillState == 3) {

			if (!anim.IsPlaying("Windmill_3")) {
				windmillState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("Windmill_4");
				windmillState = 0;
			}

		}

	}

	void Freeze() {


		if (topRockState == 0 
			&& dropState == 0
			&& powerState == 0
			&& windmillState == 0
			&& freezeState == 0) {

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
				Song.p1Combo = false;
				multiply = 0;

			}

			if (Input.GetButton ("P"+player+"_MP")) {
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;

					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}
				
				anim.Play("FreezePrep");
				freezeState = 4;
				topRockState = 0;
				dropState = 0;

			}


		}

		else if (freezeState == 1) {

			if (!anim.IsPlaying("Drop_1")) {
				freezeState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}


			if (Input.GetButton ("P"+player+"_MP")) {
				anim.Play("Drop_2");
				freezeState = 2;
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}


		}


		else if (freezeState == 2) {

			if (!anim.IsPlaying("Drop_2")) {
				freezeState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}

			if (Input.GetButton ("P"+player+"_MP")) {
				anim.Play("Drop_3");
				freezeState = 3;
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply += 1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}


		}

		else if (freezeState == 3) {

			if (!anim.IsPlaying("Drop_3")) {
				freezeState = 0;
				anim.Play("BBoyStance");
				Song.p1Combo = false;
				multiply = 0;
			}


			if (Input.GetButton ("P"+player+"_MP")) {
				anim.Play("FreezePrep");
				freezeState = 4;
				move = 1;
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					if (player == 1) {
						PowerBar.player1 += .1f;
						PowerBar.player2 -= .05f;
					} else {
						PowerBar.player1 -= .05f;
						PowerBar.player2 += .1f;
					}
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					if (player == 1) {
						Bar.player1 -= .02f;
						Bar.player2 += .02f;
					} 

					else if (player == 2) {
						Bar.player1 += .02f;
						Bar.player2 -= .02f;
					}
				}

			}

		}

		else if (freezeState == 4) {

			if (!anim.IsPlaying("FreezePrep")) {
				anim.Play("Freeze");
				move = 6;
			}

			if (Input.GetButtonUp ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				freezeState = 0;
				move = 0;
				if (Song.onBeat) {
					Song.p1Combo = true;
					multiply +=1;
					currentScore += 10 * multiply;
					
				} else {
					sprite.color = new Color(1,0,0);
					Song.p1Combo = false;
					multiply = 0;
					
				}
			}


			if (player == 1) {
				Bar.player1 += .03f * Time.deltaTime;
				Bar.player2 -= .03f * Time.deltaTime;

				if (player2Power) {
					Bar.player1 -= .02f;
					Bar.player2 += .02f;
					freezeState = 0;
				}
			} 

			else if (player == 2) {
				Bar.player1 -= .03f * Time.deltaTime;
				Bar.player2 += .03f * Time.deltaTime;

				if (player1Power) {
					Bar.player1 += .02f;
					Bar.player2 -= .02f;
					freezeState = 0;
				}
			}

			/*

			if (anim.IsPlaying("Freeze")) {
				if (Input.GetButtonDown ("P"+player+"_MP")) {
					anim.Play("BBoyStance");
					freezeState = 0;
					move = 0;
					if (Song.onBeat) {
						Song.p1Combo = true;
						multiply +=1;
						currentScore += 10 * multiply;
						if (player == 1) {
							PowerBar.player1 += .1f;
							PowerBar.player2 -= .05f;
						} else {
							PowerBar.player1 -= .05f;
							PowerBar.player2 += .1f;
						}
					} else {
						sprite.color = new Color(1,0,0);
						Song.p1Combo = false;
						multiply = 0;
						if (player == 1) {
							Bar.player1 -= .02f;
							Bar.player2 += .02f;
						} 

						else if (player == 2) {
							Bar.player1 += .02f;
							Bar.player2 -= .02f;
						}
					}

				}
			}
			*/
			

		}

	}


/*

	void Toprock() {


		if (topRockState == 0 
			&& dropState == 0
			&& windmillState == 0) {

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
			}

			if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("TopRock_1");
				topRockState = 1;
				dropState = 0;
			}


		}

		else if (topRockState == 1) {

			if (!anim.IsPlaying("TopRock_1")) {
				topRockState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("TopRock_2");
				topRockState = 2;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}

		}


		else if (topRockState == 2) {

			if (!anim.IsPlaying("TopRock_2")) {
				topRockState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("TopRock_3");
				topRockState = 3;
			}

			else if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}

		}

		else if (topRockState == 3) {

			if (!anim.IsPlaying("TopRock_3")) {
				topRockState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("TopRock_4");
				topRockState = 0;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				topRockState = 0;
			}

		}

	}

	void Drop() {


		if (dropState == 0
			&& topRockState == 0
			&& windmillState == 0) {

			

			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
			}

			if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("Drop_1");
				topRockState = 0;
				dropState = 1;
			}

		}

		else if (dropState == 1) {

			if (!anim.IsPlaying("Drop_1")) {
				dropState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("Drop_2");
				dropState = 2;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}

		}

		else if (dropState == 2) {

			if (!anim.IsPlaying("Drop_2")) {
				dropState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("Drop_3");
				down = true;
				dropState = 0;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				dropState = 0;
			}

		}

	}

	void Windmill() {


		if (down 
			&& windmillState == 0
			&& topRockState == 0
			&& dropState == 0) {


			if (!anim.IsPlaying("TopRock_4")
				&& !anim.IsPlaying("Drop_3")
				&& !anim.IsPlaying("Windmill_4")) {
				anim.Play("BBoyStance");
				down = false;
			}

			if (Input.GetAxis("P"+player+"_Horizontal") < 0 && Input.GetButtonDown("P"+player+"_HK")) {
				anim.Play("Windmill_1");
				topRockState = 0;
				dropState = 0;
				windmillState = 1;
			}

		}

		else if (windmillState == 1) {

			if (!anim.IsPlaying("Windmill_1")) {
				windmillState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetAxis("P"+player+"_Vertical") < 0 && Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("Windmill_2");
				windmillState = 0;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}

		}

		else if (windmillState == 2) {

			if (!anim.IsPlaying("Windmill_2")) {
				windmillState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetAxis("P"+player+"_Horizontal") > 0 && Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("Windmill_3");
				windmillState = 3;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}

		}

		else if (windmillState == 3) {

			if (!anim.IsPlaying("Windmill_3")) {
				windmillState = 0;
				anim.Play("BBoyStance");
			}

			if (Input.GetAxis("P"+player+"_Vertical") > 0 && Input.GetButtonDown ("P"+player+"_MK")) {
				anim.Play("Windmill_4");
				windmillState = 0;
			}

			else if (Input.GetButtonDown ("P"+player+"_LP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_LK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_HK")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}
			else if (Input.GetButtonDown ("P"+player+"_MP")) {
				anim.Play("BBoyStance");
				windmillState = 0;
			}

		}

	}

	*/

}
