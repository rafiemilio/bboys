
#pragma strict

var target : Transform;
var speed = 0.3;
var distance = 10;
var height = 10;

private var thisTransform : Transform;
private var velocity : Vector3;


function Start() {
	thisTransform = transform;
	thisTransform.parent = null;
}

function Update() {
	
	if (target == null) {
		var targetFinder : GameObject;
		targetFinder = GameObject.Find("ChosenOne(Clone)");
		if (targetFinder != null) {
			target = targetFinder.transform;
		}
		
	} else {


		thisTransform.position.x = Mathf.SmoothDamp( thisTransform.position.x, target.position.x, velocity.x, speed);
		thisTransform.position.y = Mathf.SmoothDamp( thisTransform.position.y, target.position.y + height, velocity.y, speed);
		thisTransform.position.z = Mathf.SmoothDamp( thisTransform.position.z, target.position.z - distance, velocity.z, speed);
	
	}

	
}