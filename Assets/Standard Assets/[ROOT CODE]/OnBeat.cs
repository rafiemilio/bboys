﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnBeat : MonoBehaviour {
	Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Song.onBeat) {
			image.enabled = true;
		} else {
			image.enabled = false;
		}
		
	}
}
