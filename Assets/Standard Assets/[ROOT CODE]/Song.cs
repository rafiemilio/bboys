﻿using UnityEngine;
using System.Collections;

public class Song : MonoBehaviour {
	
	AudioSource song;

	public int songChosen;
	public bool random;
	static public int chosenSong;

	//SONGS//
	public AudioClip perfectBeat;
	public AudioClip itsJustBegun;
	public AudioClip apache;
	public AudioClip mexican;
	//SONGS//

	static public float note;

	static public bool One;
	static public bool Two;
	static public bool Three;
	static public bool Four;
	static public bool onBeat;

	static public bool p1Combo;
	static public bool p2Combo;


	void Awake () {

		//STATIC RESET//
		chosenSong = 0;
		note = 0;
		One = false;
		Two = false;
		Three = false;
		Four = false;
		onBeat = false;
		p1Combo = false;
		p2Combo = false;
		//STATIC RESET//

		if (random) {
			songChosen = Random.Range(1,5);
		} 
	
		chosenSong = songChosen;
		
		song = GetComponent<AudioSource>();

		if (songChosen == 0) {
			song.clip = perfectBeat;
			note = 2.0165f;
		}
		else if (songChosen == 1) {
			song.clip = perfectBeat;
			note = 2.0165f;
		}
		else if (songChosen == 2) {
			song.clip = itsJustBegun;
			note = 2.051f;
		}
		else if (songChosen == 3) {
			song.clip = apache;
			note = 2.0865f;
		}
		else if (songChosen == 4) {
			song.clip = mexican;
			note = 2.1425f;
		}
		song.Play();
	}


}
