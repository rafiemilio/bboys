﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOut : MonoBehaviour {

	public float startAlpha = 1;
	public float alphaFade;
	Text text;
	Image sprite;
	public float fadeSpeed = .05f;

	public bool image;

	public bool startGame;

	static public bool ComboColor;

	// Use this for initialization
	void Start () {

		alphaFade = startAlpha;
		
		if (image) {
			sprite = GetComponent<Image>();
		} else {
			text = GetComponent<Text>();
		}

		if (startGame) {
			Game.started = true;
		}
	
	}
	
	void Update () {

		if (image) {
			if (Song.p1Combo) {
				sprite.color = new Color(0,0,0,alphaFade);
			} else {
				sprite.color = new Color(1,1,1,alphaFade);
			}
		} else {
			text.color = new Color(text.color.r,text.color.g,text.color.b,alphaFade);
		}

	}

	void FixedUpdate () {

		if (alphaFade > 0) {
			alphaFade -= fadeSpeed;
		}

	}
}
