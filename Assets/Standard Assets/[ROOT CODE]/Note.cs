﻿using UnityEngine;
using System.Collections;

public class Note : MonoBehaviour {

	Vector3 move;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		move = transform.localPosition;

		move.y -= Time.deltaTime/Song.note * 4;

		if (move.y < -.2f) {
			move.y = -10;
		}

		if (move.y < -10) {
			Destroy(gameObject);
		}

		transform.localPosition = move;
	
	}
}
